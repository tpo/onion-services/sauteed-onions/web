# Frequently asked questions
Below is a list of frequently asked questions.

## What is a sauteed onion?

Any TLS setup where the certificate declares an onion address.  Two SANs are
required to establish an _onion association_ from `www.example.com` to
`<addr>.onion`:

    www.example.com
    <addr>onion.www.example.com

## What does a sauteed onion do?

It makes a site's associated onion address as transparent as its TLS
certificate.  For example, this provides _forward censorship resistance_ because
TLS certificates are stored in public append-only CT logs.

## Why is it called a sauteed onion?

It is a cooking analogy.  An onion that is sauteed becomes transparent.

## Who uses sauteed onions?

We provide an [enumeration][] that is based on CT logs, and additionally operate
an exact-match [search service API][].

[enumeration]: https://www.sauteed-onions.org/db/index
[search service API]: https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/-/blob/main/doc/api.md

Example queries:

  - https://api.sauteed-onions.org/search?in=blocked.sauteed-onions.org
  - http://zpadxxmoi42k45iifrzuktwqktihf5didbaec3xo4dhvlw2hj54doiqd.onion/search?in=blocked.sauteed-onions.org

## Are sauteed onions replacing .onion addresses?

No, you still need to visit an onionsite via Tor.

If you are interested in the use of onion addresses for self-authentication and
hijack resistance in browsers without Tor access, see the work of [Syverson,
Finkel, Eskandarian, and Boneh][].

[Syverson, Finkel, Eskandarian, and Boneh]: https://doi.org/10.1145/3463676.3485610

## Are sauteed onions replacing .onion certificates?

No, you need to obtain a certificate with a `.onion` address to get [HTTPS to
your onion site][].

Similarly, sauteed onions would be orthogonal to [SOOC certificates][].

[HTTPS to your onion site]: https://community.torproject.org/onion-services/advanced/https/
[SOOC certificates]: https://github.com/alecmuffett/onion-dv-certificate-proposal/blob/master/text/draft-muffett-same-origin-onion-certificates.basic.md

## Are sauteed onions replacing SecureDrop names?

No, but like [SecureDrop names][] it would be possible to deliver a list of
sauteed onions for local querying in a web extension or natively in Tor Browser.

[SecureDrop names]: https://securedrop.org/faq/getting-onion-name-your-securedrop/

## Are sauteed onions replacing Onion-Location?

No, but "certificate-based onion location" via sauteed onions could be a future
improvement.  For example, it would be difficult to claim association with
someone else's onion address without detection.  It also works for use-cases of
[Onion-Location][] that are "not web".

We prototyped a [web extension][] that implements certificate-based
Onion-Location.

[Onion-Location]: https://community.torproject.org/onion-services/advanced/onion-location/
[web extension]: https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/webext

## Are .onion addresses part of the TLS ecosystem?

Yes, the CA/B forum voted to [allow domain validation of .onion addresses][] in
February, 2020.  Obtaining a TLS certificate with a registered domain name and a
`.onion` address is not controversial and supported by [DigiCert][] and
[HARICA][].

[DigiCert]: https://www.digicert.com/blog/ordering-a-onion-certificate-from-digicert
[HARICA]: https://news.harica.gr/article/onion_announcement/

The main reasons for defining sauteed onions based on onion addresses that are
encoded as subdomains are robustness and backwards-compatibility.  In the
future, a sauteed onion X.509v3 extension would likely be preferable.  A
lengthier discussion of the trade-offs is available in our [pre-printed
paper][].

[pre-printed paper]: https://www.sauteed-onions.org/doc/paper.pdf

Of note is that no additional certificates are issued for existing TLS sites,
but CAs need to verify one more registered domain name.  When compared to an
actual `.onion` address, CT logs need to store a handful of extra bytes per
sauteed onion setup.  We argue that this is a reasonable use of resources when
weighted towards the benefit and intended use-case: a good mechanisms to
discover onion addresses _for TLS sites that opted-in_ to resist censorship.

[allow domain validation of .onion addresses]: https://cabforum.org/2020/02/20/ballot-sc27v3-version-3-onion-certificates/

## How do I set up an onion service?

The Tor Project provides a [guide for setting up an onion service][].

You may also be interested in Alec Muffett's Enterpise Onion Toolkit ([EOTK][]).
It is a tool that deploys onion service access for existing websites.

[guide for setting up an onion service]: https://community.torproject.org/onion-services/setup/
[EOTK]: https://github.com/alecmuffett/eotk/

## How can I contribute to sauteed onions?

Reach out to say hello, provide feedback, and help with open issues.  There is
also plenty of future work, see further details in our [pre-printed paper][].

If you have a registered domain name and an associated onion address, setup
sauteed onions.
