# About
Sauteed onions is an ongoing research project.  The core contributors are:

  - Rasmus Dahlberg (Karlstad University)
  - Paul Syverson (U.S. Naval Research Laboratory)
  - Linus Nordberg (Verkligen Data AB)
  - Matthew Finkel (Independent)

Any contributions to further develop sauteed onions are most welcome.

Get in touch via IRC/Matrix or find us somewhere on the internet.
