Sauteed onions associate registered domain names with onion addresses.  These
associations are established in TLS certificates, making them publicly
enumerable in append-only [CT][] logs.

One of the most prominent use-cases of sauteed onions is to help users defeat
censorship of TLS sites: onionsites can be used _if they are discoverable_,
which is what sauteed onions help with.  This tightens the relation between
registered domain names, HTTPS, and onionsites.

[CT]: https://certificate.transparency.dev

## Search for onion addresses
You can use any existing certificate search service to check if a registered
domain name has an associated onion address.  What we will be looking for is a
domain name on the form:

    <onion addr>onion.www.example.com

Let's give it a go using [crt.sh][].

What is the onion address of `www.sauteed-onions.org`?

![Search for www.sauteed-onions.org at crt.sh.](img/landing-page.png) 

Below is the search result.

![Search results for www.sauteed-onions.org at crt.sh.](img/search-results.png) 

So, the onion address is:

    qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion

[crt.sh]: https://crt.sh

A [search service][] that is tailored specifically for sauteed onions is
available and operated by us.  Try:

  - https://api.sauteed-onions.org/search?in=blocked.sauteed-onions.org
  - http://zpadxxmoi42k45iifrzuktwqktihf5didbaec3xo4dhvlw2hj54doiqd.onion/search?in=blocked.sauteed-onions.org

[search service]: https://gitlab.torproject.org/tpo/onion-services/sauteed-onions/monitor/-/blob/main/doc/api.md

## Setup for site owners
Suppose that you have a website, for example:

    www.sauteed-onions.org

and that you have an associated onion address (if not, see [FAQ](/faq)):

    qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cyd.onion

To set up sauteed onions, you need a TLS certificate for `www.sauteed-onions.org` with the following SAN:

    qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cydonion.www.sauteed-onions.org

The required steps:

  1. Configure the above SAN in DNS.  Just like `www.sauteed-onions.org`, it
     needs to be resolvable at the time of issuing the certificate so that the
     CA can validate the domain.
  2. Follow the usual steps that are required to obtain a TLS certificate for
     `www.sauteed-onions.org`, but also add the above SAN to satisify the
     sauteed onion criteria.

Note that this works with any of today's CAs.  See some examples below.  Don't
forget step 1.

### Let's Encrypt
[Let's Encrypt][] offers free and automated certificate issuance.\
Simply use the `-d` option in [certbot][].  Here's an example using Apache:

    # do the usual certbot configuration, but list all SANs with the -d option
    $ certbot --apache -d\
        www.sauteed-onions.org,\
        qvrbktnwsztjnbga6yyjbwzsdjw7u5a6vsyzv6hkj75clog4pdvy4cydonion.www.sauteed-onions.org

When using Nginx rather than Apache, you might need to change
`/etc/nginx/nginx.conf` to increase the value of
`server_names_hash_bucket_size` to, say, 128. Don't forget to reload
Nginx.

[Let's Encrypt]: https://letsencrypt.org/
[certbot]: https://certbot.eff.org/

### HARICA
[HARICA][] issues domain validated certificates for [.onion addresses][] as well
as DNS resolvable domain names.  Login and go to server certificates.  Add your
SANs and follow the instructions.

![inserting SANs in HARICA's certificate request UX](img/harica.png)

[HARICA]: https://www.harica.gr/
[.onion addresses]: https://news.harica.gr/article/onion_announcement/
